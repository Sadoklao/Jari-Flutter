import { Router } from "express";
import HoodController from "../controller/HoodController";

import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

//Get all hood annonces
router.post("/", HoodController.listAll);
router.post("/new", HoodController.newHood);
router.delete("/:id([0-9]+)", HoodController.deleteHood);
router.post("/me/:id([0-9]+)", HoodController.getOnesByUsername);
router.post("/get",HoodController.getOneById)



// Get  Hoods  by user
// router.post("/me", HoodController.getOnesByUsername);


//Create a new user
// router.post("/", [checkJwt, checkRole(["ADMIN"])], HoodController.newHood);

//Edit one Hood
/* router.patch(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  HoodController.editHood
); */

//Delete one user
/* router.delete(
  "/:id([0-9]+)",
  [checkJwt, checkRole(["ADMIN"])],
  HoodController.deleteHood
); */

export default router;
