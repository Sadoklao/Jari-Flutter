import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as multer from "multer";
const sharp = require('sharp')
import * as helmet from "helmet";
import * as path from "path";
import * as fs from "fs";
import * as cors from "cors";
import routes from "./routes";
import { debug } from "util";
import UserController from "./controller/UserController";
import HoodController from "./controller/HoodController";
import LostAndFoundController from "./controller/LostAndFoundController";


const storage = multer.diskStorage({
  destination: function(req,file,cb)
  {
      cb(null,'../uploads/');
  },
  filename: function(req,file,cb)
  {
      cb(null,file.originalname);
  }
});
const upload = multer({storage:storage});



const storage2 = multer.diskStorage({
  destination: function(req,file,cb)
  {
      cb(null,'../uploads2/');
  },
  filename: function(req,file,cb)
  {
      cb(null,file.originalname);
  }
});
const upload2 = multer({storage:storage2});





const storage3 = multer.diskStorage({
  destination: function(req,file,cb)
  {
      cb(null,'../uploads3/');
  },
  filename: function(req,file,cb)
  {
      cb(null,file.originalname);
  }
});
const upload3 = multer({storage:storage});

//Connects to the Database -> then starts the express
createConnection()
  .then(async connection => {
    // Create a new express application instance
    const app = express();

    // Call midlewares
    app.use(cors());
    app.use(helmet());
    app.use(bodyParser.json());

    //Set all routes from routes folder
    app.use("/", routes);
    app.post('/upload',upload.single('filedata') ,async (req, res) => {
      const { filename: image } = req.file 
      let name = req.body.name;
      UserController.updatePicture(name,image)
    console.log(image);
    
      return res.send('SUCCESS!')
    })
    app.post('/uploadhood',upload2.single('filedata') ,async (req, res) => {
      const { filename: image } = req.file
      
      
      await sharp(req.file.path)
      .resize(500)
        .toFile(
          path.resolve("../uploads2/",'resized',image)
      )

      fs.unlinkSync(req.file.path);

      let id = req.body.id;
      console.log("id : "+id);
      HoodController.updatePicture(id,image)
    console.log(image);
    
      return res.send('SUCCESS!')
    })
    
    app.post('/uploadlost',upload3.single('filedata') ,async (req, res) => {
      const { filename: image } = req.file
      
      
      await sharp(req.file.path)
      .resize(500)
        .toFile(
          path.resolve("../uploads3/",'resized',image)
      )

      fs.unlinkSync(req.file.path);

      let id = req.body.id;
      console.log("id : "+id);
      LostAndFoundController.updatePicture(id,image)
    console.log(image);
    
      return res.send('SUCCESS!')
    })



    app.use('/images', express.static('../uploads/'));
    app.use('/imageshood', express.static('../uploads2/resized'));
    app.use('/imageslost', express.static('../uploads3/resized'));


    app.listen(3000, () => {
      console.log("Server started on port 3000!");
    });
  })
  .catch(error => console.log(error));


  