import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    Unique,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    Double
  } from "typeorm";
  import { Length, IsNotEmpty } from "class-validator";
  import {User} from "./User";
  
  @Entity()
 // @Unique(["username"])
  export class Lost {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    @Length(4, 5)
    type: string;

    @Column()
    name: string;

    @Column({ type: "float", precision: 10, scale: 6 })
    longitude: number;

    @Column()
    description: string;

    @Column({ type: "float", precision: 10, scale: 6 })
    latitude: number;

    @Column()
    @Length(0, 255)
    picture: string;
  
    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP'}) date: Date;

    @ManyToOne(type => User, user => user.annonces)
    user: User;

  }