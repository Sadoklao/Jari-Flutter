

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jari/entities/Session.dart';
import 'package:jari/entities/User.dart';
import 'package:jari/utils/AppNavigation.dart';
import 'package:jari/utils/App_Config.dart';
import 'package:jari/utils/Design.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:jari/widget/PopUps/PopUp.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _rememberMe = false;
  final numberController = TextEditingController();
  final passwordController = TextEditingController();
  Session session;
  bool isLoggedIn;
  User _currentUser;
  bool isLoggedInF = false;
  var profileData;
  Connectivity connectivity;
  @override
  void initState() {
    super.initState();
    session = new Session();

    _currentUser = new User();
    session.isLoggedIn().then((value) {
      // Run extra code here
      if (value) {
        AppNavigation.goToHome(context);
      }
    }, onError: (error) {
      print(error);
    });
  }

  void _Login(var username, var password) async {
    String url = AppConfig.URL_LOGIN;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"username": "$username" , "password": "$password", "typeAccount": "CLIENT"}';
    // make POST request
    var response = await post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;

    // this API passes back the id of the new item added to the body
    //print(token);
    print(statusCode);

    if (statusCode == 402) {
      final action = await Dialogs.yesAbortDialog(
          context, 'PhoneNumber', 'Wrong PhoneNumber', DialogType.error);
    } else if (statusCode == 403) {
      final action = await Dialogs.yesAbortDialog(
          context, 'Password', 'Wrong Password', DialogType.error);
    } else {
      print(response.body);
      session.setToken(response.body);
      session.setUsername(username);
      await getCurrentUser(username);
      session.setLoggedIn();

      //SharedPreferences prefs = await SharedPreferences.getInstance();
    //  print(prefs.getString('Username'));
      AppNavigation.goToIntro(context);
    }
  }

  getCurrentUser(username) async {
    String url = AppConfig.URL_GET_CURRENT_CLIENT;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json =
        '{"username": "$username"}';
    final response = await http.post(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',

    },body: json);

    int statusCode = response.statusCode;
    _currentUser =  User.fromRawJson(response.body);


     print(_currentUser);

/*

    String formattedDate = DateFoonValuermat('yyyy-MM-dd – kk:mm:ss').format(_currentUser.birthDate);
    print(formattedDate);
    */
    /* print ('Datetime:');

    print(_currentUser.createdAt);
    print ('Before upload :');
    print(_currentUser.createdAt.millisecondsSinceEpoch);
    print ('After upload :');
    DateTime dateTime = new DateTime.fromMillisecondsSinceEpoch(_currentUser.createdAt.millisecondsSinceEpoch -3600000);
    print(dateTime);*/
    // this API passes back the id of the new item added to the body
    //print(token);
    //print(statusCode);
    // _dbClient.DropTableIfExistsThenReCreate();

    //  print(_currentUser);
    // getStores(_currentUser.id);
    /*Future<User> all = _dbClient.getCurrentUser();
   all.asStream().forEach((element) => print(element));*/

    //print("sucess");
  }

  DateTime convertStringToDateTime(String date) {
    DateTime dateTime = DateTime.parse(date);

    //print(dateTime);
    //print(dateTime);
    return dateTime;
  }

  void onLoginStatusChanged(bool isLoggedIn, {profileData}) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profileData = profileData;
    });
  }
  _logout() async {
   // await facebookLogin.logOut();
    onLoginStatusChanged(false);
    print("Logged out");
  }


  OnLogInClick() async{
    if (numberController.text.isEmpty ||
        passwordController.text.isEmpty) {
      final action = await Dialogs.yesAbortDialog(
          context,
          'Fields ',
          'Complete all fields',
          DialogType.error);
    }  else {
      setState(() {
        _Login(numberController.text.trim(),
            passwordController.text.trim());
        print("Conx valid");
      });

    }
  }
  /* Design */

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: numberController,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.white,
              ),
              hintText: 'Enter your Email',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            controller: passwordController,
            obscureText: true,
            style: TextStyle(

              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hintText: 'Enter your Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () => print('Forgot Password Button Pressed'),
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Forgot Password?',
          style: kLabelStyle,
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return Container(
      height: 20.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.green,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  _rememberMe = value;
                });
              },
            ),
          ),
          Text(
            'Remember me',
            style: kLabelStyle,
          ),
        ],
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(

        elevation: 5.0,
        onPressed: () => OnLogInClick(),
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Colors.white,
        child: Text(
          'LOGIN',
          style: TextStyle(
            color: Color(0xFF527DAA),
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget _buildSignInWithText() {
    return Column(
      children: <Widget>[
        Text(
          '- OR -',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(height: 20.0),
        Text(
          'Sign in with',
          style: kLabelStyle,
        ),
      ],
    );
  }

  Widget _buildSocialBtn(Function onTap, AssetImage logo) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 60.0,
        width: 60.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0, 2),
              blurRadius: 6.0,
            ),
          ],
          image: DecorationImage(
            image: logo,
          ),
        ),
      ),
    );
  }

  Widget _buildSocialBtnRow() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _buildSocialBtn(
                () => print('Login with Facebook'),
            AssetImage(
              'assets/images/facebook.jpg',
            ),
          ),
          _buildSocialBtn(
                () => print('Login with Google'),
            AssetImage(
              'assets/images/google.jpg',
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSignupBtn() {
    return GestureDetector(
      onTap: () => AppNavigation.goToSignUp(context),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: 'Don\'t have an Account? ',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.w400,
              ),
            ),
            TextSpan(
              text: 'Sign Up',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      ThemeColors.loginGardientColor1,
                      ThemeColors.loginGardientColor2,
                      ThemeColors.loginGardientColor3,
                      ThemeColors.loginGardientColor4,

                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 120.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Sign In',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 30.0),
                      _buildEmailTF(),
                      SizedBox(
                        height: 30.0,
                      ),
                      _buildPasswordTF(),
                      _buildForgotPasswordBtn(),
                      _buildRememberMeCheckbox(),
                      _buildLoginBtn(),
                      _buildSignInWithText(),
                      _buildSocialBtnRow(),
                      _buildSignupBtn(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}