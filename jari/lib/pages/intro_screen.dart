
import 'package:flutter/material.dart';
import 'package:jari/utils/AppStrings.dart';
import 'package:jari/utils/AppNavigation.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:jari/widget/walkthrough.dart';


class IntroScreen extends StatefulWidget {
  @override
  IntroScreenState createState() {
    return IntroScreenState();
  }
}

class IntroScreenState extends State<IntroScreen> {
  final PageController controller = new PageController();
  int currentPage = 0;
  bool lastPage = false;

  void _onPageChanged(int page) {
    setState(() {
      currentPage = page;
      if (currentPage == 3) {
        lastPage = true;
      } else {
        lastPage = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            ThemeColors.loginGardientColor1,
            ThemeColors.loginGardientColor2,
            ThemeColors.loginGardientColor3,
            ThemeColors.loginGardientColor4,

          ],
          stops: [0.1, 0.4, 0.7, 0.9],
        ),
      ),
      padding: EdgeInsets.all(10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 3,
            child: PageView(
              children: <Widget>[
                Walkthrough(
                  title: AppStrings.wt1,
                  content: AppStrings.wc1,
                  imageIcon: Icons.mobile_screen_share,
                  imagecolor: ThemeColors.primaryAccentColor,
                ),
                Walkthrough(
                  title: AppStrings.wt2,
                  content: AppStrings.wc2,
                  imageIcon: Icons.search,
                  imagecolor: ThemeColors.primaryAccentColor,
                ),
                Walkthrough(
                  title: AppStrings.wt3,
                  content: AppStrings.wc3,
                  imageIcon: Icons.shopping_cart,
                  imagecolor: ThemeColors.primaryAccentColor,
                ),
                Walkthrough(
                  title: AppStrings.wt4,
                  content: AppStrings.wc4,
                  imageIcon: Icons.verified_user,
                  imagecolor: ThemeColors.primaryAccentColor,
                ),
              ],
              controller: controller,
              onPageChanged: _onPageChanged,
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  child: Text(lastPage ? "" : AppStrings.skip,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0)),
                  onPressed: () =>
                  lastPage ? null : AppNavigation.goToHome(context),
                ),
                FlatButton(
                  child: Text(lastPage ? AppStrings.gotIt : AppStrings.next,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0)),
                  onPressed: () => lastPage
                      ? AppNavigation.goToHome(context)
                      : controller.nextPage(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.easeIn),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}