import 'dart:io';
import 'package:flutter/widgets.dart';
import 'package:location/location.dart';
import 'package:path/path.dart' as path;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jari/entities/Session.dart';
import 'package:jari/entities/User.dart';
import 'package:jari/utils/App_Config.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:http/http.dart' as http;

class AddAnnouncePage extends StatefulWidget {
  @override
  _AddAnnouncePageState createState() => _AddAnnouncePageState();
}

class _AddAnnouncePageState extends State<AddAnnouncePage> {
  static const kGoogleApiKey = "AIzaSyCjIMBAXx0u0HrlpXq3aVJl-vXZRpigeRE";
  bool showPassword = false;
  Session _session;
  int selected;
  String username = "";
  Future<User> _currentUser;
  TextEditingController usernameC, emailC, addressC;
  Future<File> uploadedImage;
  File file;
  String imageName;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;
  Location location = new Location();
  final picker = ImagePicker();
  double lat, long;

  void getUserLocationData() async {

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();

    lat = _locationData.latitude;
    long = _locationData.longitude;


  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selected = -1;
    getUserLocationData();
    _session = new Session();
    _asyncMethod();
    usernameC = new TextEditingController();
    emailC = new TextEditingController();
  }

  _asyncMethod() async {
    await _session.getUsername().then((value) => {this.username = value});
    setState(() {
      this._currentUser = getCurrentUser(username);
    });
  }

  Future<User> getCurrentUser(username) async {
    String url = AppConfig.URL_GET_CURRENT_CLIENT;
    String json = '{"username": "$username"}';
    final response = await http.post(url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: json);

    int statusCode = response.statusCode;
    if (statusCode == 200) {
      print("Success");
      return Future(() => User.fromRawJson(response.body));
    }
  }

  getImageGallery() async {
    final im = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (im != null) {
        file = File(im.path);
        imageName = path.basename(file.path);
        print(imageName);
      }

      //path=image.toString().split("/");
      // print(path[path.length]);
      print("succes getting image ");
    });
  }

  getImageCamera() async {
    final im = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (im != null) {
        file = File(im.path);
        imageName = path.basename(file.path);
        print(imageName);
      }

      //path=image.toString().split("/");
      // print(path[path.length]);
      print("succes getting image ");
    });
  }

  mainBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Icon(
                  Icons.camera_alt,
                  color: ThemeColors.mainColorAccent,
                ),
                title: Text("Take Photo"),
                onTap: () {
                  Navigator.pop(context);
                  setState(() {
                    this.selected = 0;
                  });
                  getImageCamera();
                  //print(selected);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.photo_library,
                  color: ThemeColors.mainColorAccent,
                ),
                title: Text("My Images"),
                onTap: () {
                  Navigator.pop(context);
                  setState(() {
                    this.selected = 1;
                  });
                  getImageGallery();
                  print(selected);
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.clear,
                  color: ThemeColors.mainColorAccent,
                ),
                title: Text("Cancel"),
                onTap: () {
                  Navigator.pop(context);
                  setState(() {
                    this.selected = 2;
                  });
                  print(selected);
                },
              ),
            ],
          );
        });
  }

  uploadImage(name, filename) async {
    var request =
        http.MultipartRequest('POST', Uri.parse(AppConfig.URL_UPLOAD_IMAGE));
    request.fields['name'] = name;
    request.files.add(await http.MultipartFile.fromPath('image', filename));
    var res = await request.send();
    return res.reasonPhrase;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeColors.primaryAccentColor,
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: ThemeColors.whiteColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        centerTitle: true,
        title: Text(
          "Add Announcement",
          style: TextStyle(
            color: ThemeColors.whiteColor,
          ),
        ),
      ),
      body: FutureBuilder<User>(
        future: _currentUser,
        // ignore: missing_return
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.done:
              return Container(
                padding: EdgeInsets.only(left: 16, top: 25, right: 16),
                child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  child: ListView(
                    children: [
                      Center(
                        child: GestureDetector(
                          onTap: () async {
                            await mainBottomSheet(context);
                          },
                          child: PutImage(snapshot, file),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      buildTextField(
                          "Title", "Your Title Here", false, usernameC),
                      buildTextField("Description", "Your Description Here",
                          false, emailC),
                      SizedBox(
                        height: 35,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          OutlineButton(
                            padding: EdgeInsets.symmetric(horizontal: 50),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("CANCEL",
                                style: TextStyle(
                                    fontSize: 14,
                                    letterSpacing: 2.2,
                                    color: Colors.black)),
                          ),
                          RaisedButton(
                            onPressed: () {
                              Edit();
                            },
                            color: ThemeColors.primaryAccentColor,
                            padding: EdgeInsets.symmetric(horizontal: 50),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Text(
                              "SAVE",
                              style: TextStyle(
                                  fontSize: 14,
                                  letterSpacing: 2.2,
                                  color: Colors.white),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              );
              break;
            default:
              return Center(child: new CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Edit() async {
    String url = AppConfig.URL_NEW_HOODS;
    //SharedPreferences prefs = await SharedPreferences.getInstance();

    Map<String, String> headers = {"Content-type": "application/json"};

    String userName = usernameC.text.trim();
    String email = emailC.text.trim();
    String imageN = imageName;
    int id = 0;
    print(userName);
    await _currentUser.then((value) {
      id = value.id;
    });
    print(id);
    String json;

    //Control
    if (userName.isEmpty ||
        email.isEmpty ||
        imageN == null) {
      print('Fill all fields');
    } else {
      json =
          '{ "type":"Announcement","title":"$userName","content":"$email","user":"$username","longitude":"$long","latitude":"$lat"}';
    }

    print(json);
    // make POST request
    var response = await http.post(url, headers: headers, body: json);
    // check the status code for the result
    int statusCode = response.statusCode;
    if (statusCode == 204) {
      print('Success');
      if (username.isNotEmpty) {
        _session.setUsername(userName);
      }
      if (imageN != null) {
        uploadImage(imageName, file.path);
      }

      Navigator.pop(context);
    } else if (statusCode == 403) {
      print('Problem');
    } else {
      print('Error');
    }
  }
  Widget PutImage(snapshot, image) {
    return image == null
        ? Stack(
      children: [
        Container(
          width: 130,
          height: 130,
          decoration: BoxDecoration(
              border: Border.all(
                  width: 4,
                  color: Theme.of(context).scaffoldBackgroundColor),
              boxShadow: [
                BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 10,
                    color: Colors.black.withOpacity(0.1),
                    offset: Offset(0, 10))
              ],
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(
                    AppConfig.URL_GET_IMAGE + snapshot.data.picture,
                  ))),
        ),
        Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 4,
                  color: Theme.of(context).scaffoldBackgroundColor,
                ),
                color: ThemeColors.primaryAccentColor,
              ),
              child: Icon(
                Icons.edit,
                color: Colors.white,
              ),
            )),
      ],
    )
        : Stack(
      children: [
        Container(
          width: 130,
          height: 130,
          decoration: BoxDecoration(
              border: Border.all(
                  width: 4,
                  color: Theme.of(context).scaffoldBackgroundColor),
              boxShadow: [
                BoxShadow(
                    spreadRadius: 2,
                    blurRadius: 10,
                    color: Colors.black.withOpacity(0.1),
                    offset: Offset(0, 10))
              ],
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.cover, image: FileImage(file))),
        ),
        Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 4,
                  color: Theme.of(context).scaffoldBackgroundColor,
                ),
                color: ThemeColors.primaryAccentColor,
              ),
              child: Icon(
                Icons.edit,
                color: Colors.white,
              ),
            )),
      ],
    );
  }

  Widget buildTextField(String labelText, String placeholder,
      bool isPasswordTextField, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
            suffixIcon: isPasswordTextField
                ? IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.remove_red_eye,
                color: Colors.grey,
              ),
            )
                : null,
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            )),
      ),
    );
  }
}


