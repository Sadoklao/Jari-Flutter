import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:jari/utils/AppStrings.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:jari/widget/AddWidget.dart';
import 'package:jari/widget/HoodWidget.dart';
import 'package:jari/widget/LostsWidget.dart';
import 'package:jari/widget/ProfileWidget.dart';
import 'package:jari/widget/SettingsWidget.dart';


class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  int _page = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    HoodWidget(),
    LostsWidget(),
    AddWidget(),
    ProfileWidget(),
    SettingsWidget()
  ];
  @override
  void initState() {
    super.initState();


  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: ThemeColors.primaryAccentColor,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Center(
          child:  _children[_page],
        ),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        key: _bottomNavigationKey,
        backgroundColor: ThemeColors.primaryAccentColor,
        items: <Widget>[
          Image.asset('assets/images/hood.png', width: 40,height: 40,color:ThemeColors.primaryColor ,),
          Image.asset('assets/images/lf.png', width: 40,height: 40,color:ThemeColors.primaryColor ,),
          Icon(Icons.add, size: 30,color:ThemeColors.primaryColor),
          Icon(Icons.person_pin, size: 30,color:ThemeColors.primaryColor),
          Icon(Icons.menu, size: 30,color:ThemeColors.primaryColor),
        ],
        onTap: (index) {
          setState(() {
            int lastpage = _page;
            _page = index;

          });
        },
      ),

    );
  }
}
