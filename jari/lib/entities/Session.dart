import 'package:shared_preferences/shared_preferences.dart';

class Session{
  static final String KEY_IS_LOGGEDIN = "isLoggedIn";
  String token,username;
  Session({ this.token,this.username});

  Future<String>getToken() async
  {

     SharedPreferences prefs  = await SharedPreferences.getInstance() ;
      this.token= prefs.getString('token');
     return   prefs.getString('token');
  }
  Future<String>getUsername() async
  {

    SharedPreferences prefs  = await SharedPreferences.getInstance() ;
    this.username= prefs.getString('Username');
    return   prefs.getString('Username');
  }

  getMyToken(){
    getToken();
    return this.token;
  }
  setToken(String token)
  async {
     SharedPreferences prefs = await SharedPreferences.getInstance();
     this.token = token;
    prefs.setString('token', token);
    //print(prefs.getString('token'));
  }
  removeToken() async {

     SharedPreferences prefs  = await SharedPreferences.getInstance();
    prefs.remove('token');
  }
   Future<bool>isLoggedIn() async {
     SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(KEY_IS_LOGGEDIN) ?? false;
  }

  getMyUsername(){
    getUsername();
    return this.username;
  }
  setUsername(String token)
  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.username = token;
    prefs.setString('Username', token);
    //print(prefs.getString('token'));
  }
  removeUsername() async {

    SharedPreferences prefs  = await SharedPreferences.getInstance();
    prefs.remove('Username');
  }


  setLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     prefs.setBool(KEY_IS_LOGGEDIN,true);
  }
  setLoggedOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(KEY_IS_LOGGEDIN,false);
  }




}