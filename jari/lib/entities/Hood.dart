// To parse this JSON data, do
//
//     final hood = hoodFromJson(jsonString);

import 'dart:convert';

import 'User.dart';

class Hood {
  Hood({
    this.id,
    this.type,
    this.title,
    this.content,
    this.longitude,
    this.latitude,
    this.picture,
    this.date,
    this.user,
  });

  int id;
  String type;
  String title;
  String content;
  double longitude;
  double latitude;
  String picture;
  DateTime date;
  User user;

  factory Hood.fromRawJson(String str) => Hood.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Hood.fromJson(Map<String, dynamic> json) => Hood(
    id: json["id"],
    type: json["type"],
    title: json["title"],
    content: json["content"],
    longitude: json["longitude"].toDouble(),
    latitude: json["latitude"].toDouble(),
    picture: json["picture"],
    date: DateTime.parse(json["date"]),
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type": type,
    "title": title,
    "content": content,
    "longitude": longitude,
    "latitude": latitude,
    "picture": picture,
    "date": date.toIso8601String(),
    "user": user.toJson(),
  };
}


