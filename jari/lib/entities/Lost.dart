// To parse this JSON data, do
//
//     final lost = lostFromJson(jsonString);

import 'dart:convert';

import 'User.dart';

class Lost {
  Lost({
    this.id,
    this.type,
    this.name,
    this.longitude,
    this.description,
    this.latitude,
    this.picture,
    this.date,
    this.user,
  });

  int id;
  String type;
  String name;
  double longitude;
  String description;
  double latitude;
  String picture;
  DateTime date;
  User user;

  factory Lost.fromRawJson(String str) => Lost.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Lost.fromJson(Map<String, dynamic> json) => Lost(
    id: json["id"],
    type: json["type"],
    name: json["name"],
    longitude: json["longitude"].toDouble(),
    description: json["description"],
    latitude: json["latitude"].toDouble(),
    picture: json["picture"],
    date: DateTime.parse(json["date"]),
    user: User.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "type": type,
    "name": name,
    "longitude": longitude,
    "description": description,
    "latitude": latitude,
    "picture": picture,
    "date": date.toIso8601String(),
    "user": user.toJson(),
  };
}


