import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';
class User {


  User({
    this.id,
    this.username,
    this.birthDate,
    this.phoneNumber,
    this.email,
    this.address,
    this.picture,
    this.password,
    this.role,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String username;
  String birthDate;
  int phoneNumber;
  String email;
  String address;
  String picture;
  String password;
  String role;
  DateTime createdAt;
  DateTime updatedAt;

  factory User.fromRawJson(String str) => User.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    username: json["username"],
    birthDate: json["birthDate"],
    phoneNumber: json["phoneNumber"],
    email: json["email"],
    address: json["address"],
    picture: json["picture"],
    password: json["password"],
    role: json["role"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "username": username,
    "birthDate": birthDate,
    "phoneNumber": phoneNumber,
    "email": email,
    "address": address,
    "picture": picture,
    "password": password,
    "role": role,
    "createdAt": createdAt.toIso8601String(),
    "updatedAt": updatedAt.toIso8601String(),
  };

  DateTime fromTimestampToDateTime(int date) {
    DateTime dateTime = new DateTime.fromMillisecondsSinceEpoch(date);
    return dateTime;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "User {id : " +
        id.toString() +
        "\n address : " +
        address.toString() +
        "\n email : " +
        email.toString() +
        "\n image : " +
        picture.toString() +
        "\n phoneNumber : " +
        phoneNumber.toString() +

        "\n birthDate : " +
        birthDate.toString() +
        "\n createdAt : " +
        createdAt.toString() +
        "\n updatedAt : " +
        updatedAt.toString() +
        "}";
  }
}
