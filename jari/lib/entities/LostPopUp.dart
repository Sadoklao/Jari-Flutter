import 'package:flutter/material.dart';

import 'Lost.dart';

class LostMarkerPopup extends StatelessWidget {
  const LostMarkerPopup({Key key, this.lost}) : super(key: key);
  final Lost lost;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
          //  Image.network(lost.imagePath, width: 200),
            Text(lost.name),
            Text('${lost.latitude}-${lost.longitude}'),
          ],
        ),
      ),
    );
  }
}