import 'package:flutter/material.dart';
import 'package:jari/pages/home_screen.dart';
import 'package:jari/pages/intro_screen.dart';
import 'package:jari/pages/login_screen.dart';
import 'package:jari/pages/signup_screen.dart';
import 'package:jari/pages/splash_screen.dart';
import 'package:jari/widget/EditProfile.dart';


var routes = <String, WidgetBuilder>{
  "/home": (BuildContext context) => HomeScreen(),
  "/intro": (BuildContext context) => IntroScreen(),
  "/login": (BuildContext context) => LoginScreen(),
  "/signup": (BuildContext context) => SignUpScreen(),
  "/edit": (BuildContext context) => EditProfilePage(),
};

void main() => runApp(new MaterialApp(
    //theme: ThemeData(primaryColor: Colors.red, accentColor: Colors.yellowAccent),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: routes));