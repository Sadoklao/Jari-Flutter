import 'dart:ui';

import 'package:flutter/material.dart';

class ThemeColors {
  static const Color primaryColor = Color(0xff121143);
  static const Color primaryAccentColor = Color(0xff5a83c7);
  static const Color primaryThirdColor = Color.fromRGBO(58, 66, 86, 1.0);


  static const Color deepPurpleColor = Colors.deepPurple;
  static const Color redColor = Colors.red;
  static const Color amberColor = Colors.amber;
  static const Color greenColor = Colors.green;
  static const Color mainColorAccent = Color(0xff2EC4B6);
  static const Color mainColor = Color(0xff548687);
  static const Color blueGreyColor = Colors.blueGrey;
  static const Color darkBlueColor = Color(0xff30394B);
  static const Color whiteColor = Colors.white;
  static const Color backgroundColor = Color(0xffe8f2fc);
  static const Color greyColor = Colors.grey;
  static const Color shadowColor = Colors.black26;
  static const Color blackColor = Colors.black;
  static const Color cardBlackColor = Color(0xff202426);
  static const Color cardGreyColor = Color(0xff3D484D);

  static const Color loginGardientColor1 = Color(0xff5a83c7);
  static const Color loginGardientColor2 = Color(0xff425a99);
  static const Color loginGardientColor3 = Color(0xff2a346d);
  static const Color loginGardientColor4 = Color(0xff121143);

  //background-image: linear-gradient(to left bottom, #121143, #2a346d, #425a99, #5a83c7, #73aef5);
}
