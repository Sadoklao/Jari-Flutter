import 'package:flutter/material.dart';

class AppNavigation {
  static void goToHome(BuildContext context) {
    Navigator.pushReplacementNamed(context, "/home");
  }

  static void goToIntro(BuildContext context) {
    Navigator.pushReplacementNamed(context, "/intro");
  }

  static void goToLogin(BuildContext context) {
    Navigator.pushReplacementNamed(context, "/login");
  }
  static void goToSignUp(BuildContext context) {
    Navigator.pushReplacementNamed(context, "/signup");
  }

  static void goToEdit(BuildContext context) {
    Navigator.pushNamed(context, "/edit");
  }
}