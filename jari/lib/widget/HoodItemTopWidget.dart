

import 'package:flutter/material.dart';
import 'package:jari/helper/translations.dart';
import 'package:jari/utils/ThemeColors.dart';

class HoodItemTopWidget extends StatefulWidget {
  @override
  _HoodItemTopWidgetState createState() =>
      _HoodItemTopWidgetState();
}

class _HoodItemTopWidgetState extends State<HoodItemTopWidget> {
  String dropdownValue = "Recent";

  String Recent, Oldest, Highest, Lowest;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // dropdownValue = Translations.of(context).text("Recent");
//    Recent= Translations.of(context).text("Recent");
//    Oldest= Translations.of(context).text("Oldest");
//    Highest= Translations.of(context).text("Highest");
//    Lowest= Translations.of(context).text("Lowest");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 25.0),
      decoration: new BoxDecoration(
        color: ThemeColors.whiteColor,
        borderRadius: new BorderRadius.only(
          topLeft: Radius.circular(32),
          topRight: Radius.circular(32),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 8, bottom: 8, left: 20, right: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[

            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Sort By',
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                ),

              ],
            )
          ],
        ),
      ),
    );
  }
}
