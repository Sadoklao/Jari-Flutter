import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:jari/entities/Hood.dart';
import 'package:jari/entities/Lost.dart';
import 'package:jari/entities/LostPopUp.dart';
import 'package:jari/utils/App_Config.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:latlong/latlong.dart';
import 'package:location/location.dart';
import 'package:user_location/user_location.dart';
import 'package:http/http.dart' as http;

class LostsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LostsWidgetPage();
  }
}

class LostsWidgetPage extends StatefulWidget {
  @override
  _LostsWidgetPageState createState() => _LostsWidgetPageState();
}

class _LostsWidgetPageState extends State<LostsWidgetPage> {
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;
  Location location = new Location();

  bool _loading = true;
  // ADD THIS
  MapController mapController = MapController();
  UserLocationOptions userLocationOptions;
  // ADD THIS
  List<Marker> markers = [];
  List<Lost> Lostss = [];
  void getLostData() async {
    double lat, long;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();

    lat = _locationData.latitude;
    long = _locationData.longitude;

    String body = '{ "long":"$lat","latt":"$long"}';
    String url = AppConfig.URL_GET_LOSTS;
    final response = await http.post(url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: body);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      List data = json.decode(response.body);
      // print(data);
      List<Lost> Losts = data.map((json) => Lost.fromJson(json)).toList();

      setState(() {
        Losts.forEach((element) {
          LatLng p = new LatLng(element.latitude, element.longitude);
          markers.add(new Marker(
            point: p,
            builder: (ctx)  {
              if(element.type=="Lost"){
                return Icon(Icons.vpn_key,size: 35,color: ThemeColors.redColor,);

              }else{
                return Icon(Icons.shopping_basket,size: 35,color: ThemeColors.greenColor,);
              }
            }
          ));
        });
        _loading =false;
        this.Lostss.addAll(Losts);
      });
      print("Losts Fetched from server ");
    } else {
      print("Else ");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLostData();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final PopupController _popupLayerController = PopupController();


    userLocationOptions = UserLocationOptions(
      context: context,
      markers: markers,

    );
    if(_loading){
      return CircularProgressIndicator();
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: ThemeColors.primaryAccentColor,
        body: Container(
          height: size.height,
          child: FlutterMap(
            options: new MapOptions(
              center: LatLng(_locationData.latitude, _locationData.longitude),
             // zoom: 13.0,

              interactive: true,
              onTap: (_) => _popupLayerController.hidePopup(),
              plugins: [
                // ADD THIS
                MarkerClusterPlugin(),
                UserLocationPlugin(),
                PopupMarkerPlugin()
              ],
            ),
            layers: [
              new TileLayerOptions(
                urlTemplate:
                    "https://api.mapbox.com/styles/v1/sadoklao/ckhqio9c3132j19keqkpl0iiq/tiles/256/{z}/{x}/{y}@2x?access_token={accessToken}",
                additionalOptions: {
                  'accessToken':
                      'pk.eyJ1Ijoic2Fkb2tsYW8iLCJhIjoiY2toa3dncGo3MDFpMzM4bXhvNjdjNzZyayJ9.2c9awenNLf_pUIBliOYi9Q',
                  'id': 'mapbox.streets',
                },
              ),
            /*  MarkerClusterLayerOptions(
                maxClusterRadius: 120,
                size: Size(40, 40),
                fitBoundsOptions: FitBoundsOptions(
                  padding: EdgeInsets.all(50),
                ),
                markers: markers,

                polygonOptions: PolygonOptions(
                    borderColor: Colors.blueAccent,
                    color: Colors.black12,
                    borderStrokeWidth: 3),
                builder: (context, markers) {
                  return FloatingActionButton(
                    child: Text(markers.length.toString()),
                    onPressed: null,
                  );
                },
              ),*/

              PopupMarkerLayerOptions(
                markers: markers,
                popupSnap: PopupSnap.top,
                popupController: _popupLayerController,
                popupBuilder: (BuildContext _, Marker marker) {
                 return ExamplePopup(marker,this.Lostss);
                } ,
              ),
              // ADD THIS
              //MarkerLayerOptions(markers: markers),
              // ADD THIS
              userLocationOptions,
            ],
            // ADD THIS
            mapController: mapController,
          ),
        ),
      ),
    );
  }


}

class ExamplePopup extends StatefulWidget {
  final Marker marker;
  final List<Lost> Losts;
  ExamplePopup(this.marker,this.Losts, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ExamplePopupState(this.marker,this.Losts);
}

class _ExamplePopupState extends State<ExamplePopup> {
  final Marker _marker;
  final List<Lost> _Losts;
  final List<IconData> _icons = [
    Icons.star_border,
    Icons.star_half,
    Icons.star
  ];
  int _currentIcon = 0;
  Lost _curr ;
  _ExamplePopupState(this._marker,this._Losts);

  @override
  Widget build(BuildContext context) {

    _Losts.forEach((element) {
      LatLng p = new LatLng(element.latitude, element.longitude);
        if(this._marker.point == p){
        _curr= element;
      }
    });


    return Card(
      child: InkWell(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 20, right: 10),
              child: Icon(_icons[_currentIcon]),
            ),
            _cardDescription(context),
          ],
        ),
        onTap: () => setState(() {
          _currentIcon = (_currentIcon + 1) % _icons.length;
        }),
      ),
    );
  }

  Widget _cardDescription(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              this._curr.name,
              overflow: TextOverflow.fade,
              softWrap: false,
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14.0,
              ),
            ),
            const Padding(padding: EdgeInsets.symmetric(vertical: 4.0)),
            Text(
              "Description: "+this._curr.description,
              style: const TextStyle(fontSize: 12.0),
            ),
            Text(
              "Type: "+this._curr.type,
              style: const TextStyle(fontSize: 12.0),
            ),
          ],
        ),
      ),
    );
  }
}
