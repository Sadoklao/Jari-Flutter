import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jari/entities/Hood.dart';
import 'package:jari/pages/NewAnnounde.dart';
import 'package:jari/utils/Config_size.dart';
import 'file:///E:/GitHub/Jari-Flutter/jari/lib/widget/AnimatedSearchBar.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:jari/utils/constants.dart';
import 'package:jari/widget/CustomDropDown.dart';

class AddWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AddWidgetPage();
  }
}

class AddWidgetPage extends StatefulWidget {
  @override
  _AddWidgetPageState createState() => _AddWidgetPageState();
}

class _AddWidgetPageState extends State<AddWidgetPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ThemeColors.primaryAccentColor,
        elevation: 5,
        centerTitle: true,
        title: Text(
          "New",
          style: TextStyle(
            color: ThemeColors.whiteColor,
          ),
        ),
      ),
      backgroundColor: ThemeColors.primaryAccentColor,
      body: buildBody(),
    );
  }

  Widget buildBody() {
    double defaultSize = SizeConfig.defaultSize;
    return Column(
      children: [
        SizedBox(
          height: defaultSize * 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () => {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddAnnouncePage()))
              },
              child: Container(
                width: defaultSize * 16,
                height: defaultSize * 16,
                child: Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.announcement,
                        size: defaultSize * 5,
                        color: ThemeColors.primaryColor,
                      ),
                      SizedBox(height: defaultSize),
                      Text(
                        "Announcement",
                        style: TextStyle(
                            fontSize: defaultSize * 2,
                            color: ThemeColors.primaryColor,
                            fontWeight: FontWeight.w500),
                      )
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: ThemeColors.whiteColor,
                  elevation: 5,
                ),
              ),
            ),
            SizedBox(
              width: defaultSize * 5,
            ),
            CustomDropdown(text: "Call to nothing")
          ],
        ),
      ],
    );
  }
}
