import 'dart:convert';

import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:jari/entities/Hood.dart';
import 'package:jari/entities/Session.dart';
import 'package:jari/entities/User.dart';
import 'package:jari/utils/App_Config.dart';
import 'FilterButtonWidget.dart';
import 'HoodItemTopWidget.dart';
import 'file:///E:/GitHub/Jari-Flutter/jari/lib/widget/AnimatedSearchBar.dart';
import 'file:///E:/GitHub/Jari-Flutter/jari/lib/widget/CardHoodItem.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:jari/utils/constants.dart';
import 'package:filter_list/filter_list.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';

class HoodWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HoodWidgetPage();
  }
}

class HoodWidgetPage extends StatefulWidget {
  @override
  _HoodWidgetPageState createState() => _HoodWidgetPageState();
}

class _HoodWidgetPageState extends State<HoodWidgetPage> {
  final CategoriesScroller categoriesScroller = CategoriesScroller();
  ScrollController controller = ScrollController();
  bool closeTopContainer = false;
  double topContainer = 0;
  Position _currentPosition;
  String _currentAddress;
  Session _session;
  String username = "";
  Location location = new Location();
  List<Widget> itemsData = [];
  List<dynamic> responseList = FOOD_DATA;
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;
  bool _loading = true;
  TextEditingController textEditingController = new TextEditingController();
  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: ThemeColors.primaryAccentColor,
      leading: SizedBox(),
      // On Android it's false by default
      centerTitle: true,
      title: Text(
        "Announces",
        style: TextStyle(
          color: ThemeColors.whiteColor,
        ),
      ),
      actions: <Widget>[
        IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.history,
            color: ThemeColors.whiteColor,
          ),
        ),
      ],
    );
  }

  void getHoodData() async {
    List<Widget> listItems = [];
    double lat, long;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();

    lat = _locationData.latitude;
    long = _locationData.longitude;

    String body = '{ "long":"$lat","latt":"$long"}';
    String url = AppConfig.URL_GET_HOODS;
    final response = await http.post(url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: body);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      List data = json.decode(response.body);
      // print(data);
      List<Hood> hoods = data.map((json) => Hood.fromJson(json)).toList();
      hoods.forEach((hood) {
        listItems.add(CardHoodItem(
          hood: hood,
        ));
      });
      print(hoods.length);
      setState(() {
        itemsData = listItems;
        _loading = false;
      });
      print("hoods Fetched from server ");
    } else {
      print("Else ");
    }
  }

  @override
  void initState() {
    super.initState();

    _session = new Session();
    getHoodData();
    controller.addListener(() {
      double value = controller.offset / 119;

      setState(() {
        topContainer = value;
        closeTopContainer = controller.offset > 50;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final double categoryHeight = size.height * 0.30;
    if (_loading) {
      return CircularProgressIndicator();
    }
    return SafeArea(
      child: Scaffold(
        appBar: buildAppBar(),
        backgroundColor: ThemeColors.primaryAccentColor,
        body: Container(
          height: size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AnimatedOpacity(
                duration: const Duration(milliseconds: 200),
                opacity: closeTopContainer ? 0 : 1,
                child: AnimatedContainer(
                    duration: const Duration(milliseconds: 200),
                    width: size.width,
                    alignment: Alignment.topCenter,
                    height: closeTopContainer ? 0 : categoryHeight,
                    child: Column(
                      children: [
                        categoriesScroller,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [AnimatedSearchBar(), FilterButtonWidget()],
                        )
                      ],
                    )),
              ),
              Expanded(
                  child: ListView.builder(
                      controller: controller,
                      itemCount: itemsData.length,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (context, index) {
                        double scale = 1.0;
                        if (topContainer > 0.5) {
                          scale = index + 0.5 - topContainer;
                          if (scale < 0) {
                            scale = 0;
                          } else if (scale > 1) {
                            scale = 1;
                          }
                        }
                        return Opacity(
                          opacity: scale,
                          child: Transform(
                            transform: Matrix4.identity()..scale(scale, scale),
                            alignment: Alignment.bottomCenter,
                            child: Align(
                                heightFactor: 0.7,
                                alignment: Alignment.topCenter,
                                child: itemsData[index]),
                          ),
                        );
                      })),
            ],
          ),
        ),
      ),
    );
  }
}

class CategoriesScroller extends StatelessWidget {
  const CategoriesScroller();

  @override
  Widget build(BuildContext context) {
    final double categoryHeight =
        MediaQuery.of(context).size.height * 0.30 - 100;
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          children: [
            FittedBox(
              fit: BoxFit.fill,
              alignment: Alignment.topCenter,
              child: Row(
                children: <Widget>[
                  Container(
                    width: 150,
                    margin: EdgeInsets.only(right: 20),
                    height: categoryHeight,
                    decoration: BoxDecoration(
                        color: ThemeColors.primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Most\nFavorites",
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "20 Items",
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: 150,
                    margin: EdgeInsets.only(right: 20),
                    height: categoryHeight,
                    decoration: BoxDecoration(
                        color: Colors.blue.shade400,
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Newest",
                              style: TextStyle(
                                  fontSize: 25,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "20 Items",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 150,
                    margin: EdgeInsets.only(right: 20),
                    height: categoryHeight,
                    decoration: BoxDecoration(
                        color: Colors.lightBlueAccent.shade400,
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Super\nSaving",
                            style: TextStyle(
                                fontSize: 25,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "20 Items",
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
