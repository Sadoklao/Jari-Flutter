import 'package:flutter/material.dart';
import 'package:jari/utils/ThemeColors.dart';

// ignore: must_be_immutable
class AnimatedSearchBar extends StatefulWidget {
  TextEditingController controller;

  @override
  _AnimatedSearchBarState createState() => _AnimatedSearchBarState(this.controller);
}

class _AnimatedSearchBarState extends State<AnimatedSearchBar> {
  bool _folded = true;
  TextEditingController controller;
  _AnimatedSearchBarState(this.controller);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 400),
      width: _folded ? 56 : 200,
      height: 56,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(32),
        color: Colors.white,
        boxShadow: kElevationToShadow[6],
      ),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 16),
              child: !_folded
                  ? TextField(
                controller: this.controller,
                decoration: InputDecoration(
                    hintText: 'Search',
                    hintStyle: TextStyle(color: ThemeColors.primaryColor),
                    border: InputBorder.none),
              )
                  : null,
            ),
          ),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: InkWell(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(_folded ? 32 : 0),
                  topRight: Radius.circular(32),
                  bottomLeft: Radius.circular(_folded ? 32 : 0),
                  bottomRight: Radius.circular(32),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Icon(
                    _folded ? Icons.search : Icons.close,
                    color: ThemeColors.primaryColor,
                  ),
                ),
                onTap: () {
                  setState(() {
                    this.controller.clear();
                    _folded = !_folded;
                  });
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}