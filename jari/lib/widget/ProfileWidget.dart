import 'package:async/async.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jari/entities/Hood.dart';
import 'package:jari/entities/Session.dart';
import 'package:jari/entities/User.dart';
import 'package:jari/utils/AppNavigation.dart';
import 'package:jari/utils/App_Config.dart';
import 'package:jari/utils/Config_size.dart';
import 'package:jari/utils/ThemeColors.dart';
import 'package:jari/widget/profile_menu_item.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'Info.dart';

class ProfileWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  Session _session;
  String username = "";
  Future<User> _currentUser;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _session = new Session();
    _asyncMethod();

    //_currentUser = await
  }

  _asyncMethod() async {
    await _session.getUsername().then((value) => {this.username = value});
    setState(() {
      this._currentUser = getCurrentUser(username);
    });
  }

  Future<User> getCurrentUser(username) async {
    String url = AppConfig.URL_GET_CURRENT_CLIENT;
    String json = '{"username": "$username"}';
    final response = await http.post(url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: json);

    int statusCode = response.statusCode;
    if (statusCode == 200) {
      print("Success");
      return Future(() => User.fromRawJson(response.body));
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBody(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: ThemeColors.primaryAccentColor,
      leading: SizedBox(),
      elevation: 5,
      // On Android it's false by default
      centerTitle: true,
      title: Text(
        "Profile",
        style: TextStyle(
          color: ThemeColors.whiteColor,
        ),
      ),
      actions: <Widget>[
        IconButton(
          onPressed: () {
            AppNavigation.goToEdit(context);
          },
          icon: Icon(
            Icons.edit,
            color: ThemeColors.whiteColor,
          ),
        ),
      ],
    );
  }

  Widget buildBody() {
    double defaultSize = SizeConfig.defaultSize;
    return SingleChildScrollView(
        child: FutureBuilder<User>(
            future: _currentUser,
            // ignore: missing_return
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Center(child: new CircularProgressIndicator());
                  break;
                case ConnectionState.active:
                  return Center(child: new CircularProgressIndicator());
                  break;
                case ConnectionState.done:
                  return Column(
                    children: <Widget>[
                      Info(
                        image: AppConfig.URL_GET_IMAGE+snapshot.data.picture,
                        name: snapshot.data.username.toString(),
                        email: "joined 2 months ago",
                      ),
                      SizedBox(height: SizeConfig.defaultSize * 2), //20
                      ProfileMenuItem(
                        iconSrc: Icon(
                          Icons.phone,
                          size: defaultSize * 2,
                          color: ThemeColors.primaryAccentColor,
                        ),
                        title: "+216 " + snapshot.data.phoneNumber.toString(),
                      ),
                      ProfileMenuItem(
                        iconSrc: Icon(
                          Icons.cake,
                          size: defaultSize * 2,
                          color: ThemeColors.primaryAccentColor,
                        ),
                        title: snapshot.data.birthDate.toString(),
                      ),
                      ProfileMenuItem(
                        iconSrc: Icon(
                          Icons.email,
                          size: defaultSize * 2,
                          color: ThemeColors.primaryAccentColor,
                        ),
                        title: snapshot.data.email.toString(),
                      ),
                      ProfileMenuItem(
                        iconSrc: Icon(
                          Icons.my_location,
                          size: defaultSize * 2,
                          color: ThemeColors.primaryAccentColor,
                        ),
                        title: snapshot.data.address.toString(),
                      ),
                      ProfileMenuItem(
                        iconSrc: Icon(
                          Icons.date_range,
                          size: defaultSize * 2,
                          color: ThemeColors.primaryAccentColor,
                        ),
                        title: snapshot.data.createdAt.toString(),
                      ),
                      ProfileMenuItem(
                        iconSrc: Icon(
                          Icons.update,
                          size: defaultSize * 2,
                          color: ThemeColors.primaryAccentColor,
                        ),
                        title: snapshot.data.updatedAt.toString(),
                      ),
                    ],
                  );
                  break;
                default:
                  return Center(child: new CircularProgressIndicator());
              }
            }));
  }
}
