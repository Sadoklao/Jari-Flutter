import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:jari/entities/Hood.dart';
import 'package:jari/utils/ThemeColors.dart';

import '../utils/LoadImage.dart';
import '../utils/Utils.dart';

class CardHoodItem extends StatelessWidget {
  Hood hood ;

  CardHoodItem({this.hood});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12.0),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(color: Colors.black.withAlpha(100), blurRadius: 10.0),
            ]),
        child: ListTile(
          title: Text(
            Utils.capitalizeSentence(this.hood.title),
            style:
            TextStyle(fontWeight: FontWeight.w500, color: ThemeColors.blackColor),
          ),
          leading: Icon(
            Icons.icecream
          ),
          subtitle: new RichText(
            text: TextSpan(
              style:
              TextStyle(fontWeight: FontWeight.w600, color: ThemeColors.blackColor),
              children: <TextSpan>[
                TextSpan(
                  text: Utils.convertDateFromString(
                      this.hood.date.toString(), "MMM dd"),
                ),
                TextSpan(
                    text: ', at ',
                    style: TextStyle(
                        fontWeight: FontWeight.w400, color: Colors.black54)),
                TextSpan(
                  text: Utils.convertDateFromString(
                      this.hood.date.toString(), "hh:mm a"),
                ),
              ],
            ),
          ),
          //trailing: getPointsOrder(),
        ),
      ),
    );
  }


}
