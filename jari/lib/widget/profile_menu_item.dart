import 'package:flutter/material.dart';
import 'package:jari/utils/Config_size.dart';

import 'package:jari/utils/ThemeColors.dart';


class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({
    Key key,
    this.iconSrc,
    this.title,

  }) : super(key: key);
  final Widget iconSrc;
  final String title;


  @override
  Widget build(BuildContext context) {
    double defaultSize = SizeConfig.defaultSize;
    return InkWell(
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: defaultSize * 2, vertical: defaultSize * 2),
        child: SafeArea(
          child: Row(
            children: <Widget>[
              iconSrc,
              SizedBox(width: defaultSize * 2),
              Text(
                title,
                style: TextStyle(
                  fontSize: defaultSize * 1.8, //18
                  color: ThemeColors.primaryAccentColor,
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}