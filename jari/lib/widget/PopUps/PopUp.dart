
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:jari/utils/ThemeColors.dart';

enum DialogAction { yes, abort }
enum DialogType { error, warning, success, offline, online }

class Dialogs {
  static Future<DialogAction> yesAbortDialog(
    BuildContext context,
    String title,
    String body,
    DialogType type,
  ) async {
    final action = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Text(title),
          content: Container(
            height: 200,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                getImageFromType(type),
                SizedBox(
                  height: 10,
                ),
                Text(
                  body,
                  style: TextStyle(
                    color: ThemeColors.blackColor,
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    fontFamily: 'Playfair',
                  ),
                ),
                //Text(body),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              onPressed: () => Navigator.of(context).pop(DialogAction.yes),
              color: getColorFromType(type),
              child: const Text(
                'OK',
                style: TextStyle(
                  color: ThemeColors.whiteColor,
                ),
              ),
            ),
          ],
        );
      },
    );
    return (action != null) ? action : DialogAction.abort;
  }

  static Widget getImageFromType(DialogType type) {
    switch (type) {
      case DialogType.error:
        return FaIcon(
          FontAwesomeIcons.solidTimesCircle,
          color: ThemeColors.redColor,
          size: 120,
        );
        break;
      case DialogType.warning:
        return FaIcon(
          FontAwesomeIcons.exclamationCircle,
          color: ThemeColors.amberColor,
          size: 120,
        );
        break;
      case DialogType.success:
        return FaIcon(
          FontAwesomeIcons.solidCheckCircle,
          color: ThemeColors.greenColor,
          size: 120,
        );
        break;
      case DialogType.offline:
        return Icon(
          Icons.signal_wifi_off,
          color: ThemeColors.redColor,
          size: 120,
        );
        break;
      case DialogType.online:
        return FaIcon(
          FontAwesomeIcons.wifi,
          color: ThemeColors.greenColor,
          size: 120,
        );
        break;
    }
  }

  static Color getColorFromType(DialogType type) {
    switch (type) {
      case DialogType.error:
        return ThemeColors.redColor;
        break;
      case DialogType.warning:
        return ThemeColors.amberColor;
        break;
      case DialogType.success:
        return ThemeColors.greenColor;
        break;
      case DialogType.offline:
        return ThemeColors.redColor;
        break;
      case DialogType.online:
        return ThemeColors.greenColor;
        break;
    }
  }
}
