import 'package:flutter/material.dart';
import 'package:jari/utils/ThemeColors.dart';

class FilterButtonWidget extends StatefulWidget {
  @override
  _AnimatedSearchBarState createState() => _AnimatedSearchBarState();
}

class _AnimatedSearchBarState extends State<FilterButtonWidget> {
  bool _folded = true;
  String dropdownValue = "Recent";

  String Recent, Oldest, Highest, Lowest;
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        decoration: new BoxDecoration(
          color: ThemeColors.whiteColor,
          boxShadow: kElevationToShadow[6],
          borderRadius: new BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: DropdownButton<String>(
            value: dropdownValue,
            elevation: 0,
            underline: Container(
              color: ThemeColors.backgroundColor,
            ),
            style: TextStyle(color: ThemeColors.primaryColor),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: <String>['Oldest', 'Recent', 'Highest', 'Lowest']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(
                    fontSize: 14,
                    color: ThemeColors.primaryColor,
                  ),
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}