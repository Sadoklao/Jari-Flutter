<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->

</p>
<h1 align="center">
  Jari Flutter
</h1>

This App is actually a platform for Lost & Founds as well as neighbourhood announces using Flutter (Dart) for a Front end and Node Js TypeOrm for a Backend.

---

**Table of Contents**

- [Library Used](#library-used)
- [Installation Guide](#installation-guide)
- [Demo](#demo)

## Library Used

The Front End Library used in Flutter are :

```yml
cached_network_image: ^2.0.0
shimmer: ^1.0.1
curved_navigation_bar: ^0.3.4
font_awesome_flutter: ^8.0.1
flappy_search_bar: 1.4.1
filter_list: ^0.0.1
flutter_map: ^0.10.1+1
mobx: ^0.3.5
flutter_mobx: ^0.3.0+1
shared_preferences: ^0.5.3+4
flutter_beautiful_popup: ^1.5.0
async: ^2.4.2
image_picker: ^0.6.0+3
geolocator: ^5.3.1
latlong: ^0.6.1
location: ^3.0.0
flutter_map_marker_popup: any
flutter_map_marker_cluster: any

user_location:
  git:
    url: https://github.com/igaurab/user_location_plugin.git
connectivity:
cupertino_icons: ^1.0.0
```

The Back End Library used in NodeJS are :

```json
"dependencies": {
      "@types/multer": "^1.3.10",
      "@types/sharp": "^0.23.1",
      "bcryptjs": "^2.4.3",
      "body-parser": "^1.18.1",
      "class-validator": "^0.11.0",
      "cors": "^2.8.5",
      "express": "^4.15.4",
      "geolib": "^3.2.0",
      "googlemaps": "^1.12.0",
      "helmet": "^3.21.2",
      "here-maps-node": "0.0.2",
      "jsonwebtoken": "^8.5.1",
      "multer": "^1.4.2",
      "mysql": "^2.14.1",
      "reflect-metadata": "^0.1.10",
      "sharp": "^0.23.4",
      "ts-node-dev": "^1.0.0-pre.44",
      "typeorm": "0.2.20"
   },

```

## Installation Guide

Steps to run BackEnd project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command


## Demo

-See [Screen Captures](images).
